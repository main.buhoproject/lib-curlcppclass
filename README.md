cURL C++ class library (curlcppclass)
======================

[![License](https://img.shields.io/badge/license-GPL-blue.svg)](https://opensource.org/licenses/GPL-2.0)

### Description

The lib-curlcppclass (cURL C ++ class library) is based on the well-known curl library.
The goal of this project is to have a modern class library to use curl in c++ applications.
This project started with the need of a SMTP curl c++ class, but is possible to go further.
Particularly to make smtp-curl-class C++ compliance C++11/14/17 and beyond.

This project was born as a needed part of buho project. 
buho project is a MVC  architecture web application in abbreviated C++ with a few lines.

### Classes
* curl_smtp is a C++ class based on cURL to manage outgoing mail.
* more in the future.

### Features
* SMTP mail with the known curl basic configuration.
* Posibility of using gmail acount to send email throw smtp.gmail.com socket.
* RFC 5322 compliance.
* TLS / OpenSSL.

### Example
The smtp example refers to using the smtp google's server. To get it work take in consideration
that gmails configuration account must allow the use of unsecured applications.


To enabling less secure apps to access Gmail open your Google Admin console. 
Click Security > 'Basic settings' and under 'Less secure apps', select Go to settings for less secure apps. 
Select the 'Enforce access to less secure apps'.


### Usage

See [smtp_example.cpp](smtp_example.cpp) for example usage. 

### Dependencies

* curl
* OpenSSL libraries

### Compile

Compiled with eclipse with a C++11 supported compiler:

```sh
compiled from eclipse
```

#### Run example

```sh
./smtp_example
```


