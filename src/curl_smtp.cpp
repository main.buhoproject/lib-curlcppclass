/*
 *  curl_smtp.cpp
 *
 *  Created on	: 14 mar. 2020
 *  Author		: Casiano Carlos Budden
 *	email		: main.buhoproject@gmail.com, info.xunix@gmail.com,
 *
 *
					GNU GENERAL PUBLIC LICENSE
                       Version 2, June 1991

 Copyright (C) 1989, 1991 Free Software Foundation, Inc.,
 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
 Everyone is permitted to copy and distribute verbatim copies
 of this license document, but changing it is not allowed.

 *
 */

#include "curl_smtp.hpp"
#include <algorithm>
#include <stdexcept>






/// Xxxx xxx...
/***********************************************************************************************//**
 - Description: xx.
 - Arguments:
 @-Returning values:
 @-Note:
 @-Example:
****************************************************************************************************/
size_t curl_smtp::read_callback( void *contents, size_t size, size_t nmemb, void *userdata )
{
	stringdata *text = reinterpret_cast<stringdata *>(userdata);

	if( (size == 0) || (nmemb == 0) || ((size*nmemb) < 1) || (text->bytesleft == 0) ){
		return 0;
	}

	if( (nmemb * size) >= text->msg.size() ){
		text->bytesleft = 0;
		return text->msg.copy(reinterpret_cast<char *>(contents), text->msg.size());
	}

	//To access a method into static one: static_cast<curl_smtp*>(userdata)->stringCat();
	return 0;
}


/* For the future: Anyone is invited to contribute.
Write_callback(){
	size_t realsize = size * nmemb;
	curl_smtp *rq = static_cast<curl_smtp*>( userdata );
	std::uint8_t *b = static_cast<std::uint8_t*>( contents );
	std::uint8_t *e = b + realsize;
	std::copy( b, e, std::back_inserter( rq->m_data ) );			//src_From, src_To, target_start
}*/




/// Concatenates the  vector of strings into one string...
/***********************************************************************************************//**
 - Description: Concatenates the the vector (vecLine) of strings into one string and sores each character
 	 	 	 	into a uint8_t vector, with the possibility of encoding the stored chars.
 - Arguments:
 @-Returning values:
 @-Note:			 	 	 THIS IS EXPERIMENTAL AND ACTUALY CHANGING.
 @-Example:
****************************************************************************************************/
void curl_smtp::stringCat( void )
{
	/*
	m_szAll="";

	iterL = vecLine.begin();
	while( iterL != vecLine.end() ){

		switch( m_nEncodeType ){
			case 0:
				m_szAll += (*iterL);
			break;

			case 1:
			break;

			default:
				m_szAll += (*iterL);
			break;
		};
	iterL++;
	}

	//std::cout << "szAll: " << szAll << std::endl;
	std::vector<uint8_t> vect(m_szAll.begin(), m_szAll.end());
	m_data = vect;
*/

}


/// Xxxx xxx...
/***********************************************************************************************//**
 - Description: xx.
 - Arguments:
 @-Returning values:
 @-Note:
 @-Example:
****************************************************************************************************/
void curl_smtp::generateEMail()
{
	m_szAll = 	("Date: " + get_DateTime() + "\r\n")+
				("X-Mailer: " + m_szAppName + "\r\n")+
				("From: <" + m_szFrom+ ">\r\n")+
				("To: <" + m_szTo +">\r\n");

	if( m_szCC != "" )
		m_szAll += ("Cc: <" + m_szCC + ">\r\n");

	if( m_szMessageID != "" )
		m_szAll += ("Message-ID: " + m_szMessageID + "\r\n");

	if( m_szMimeVersion != "" )
		m_szAll += ("Mime-Version: " + m_szMimeVersion + "\r\n");

	if( m_szContentType != "" &&   m_szCharset != "")
		m_szAll += ("Content-Type: " + m_szContentType + "; charset=\""+ m_szCharset + "\"" + "\r\n");


	m_szAll += ("Subject: " + m_szSubject + "\r\n")+
	("\r\n")+// empty line to divide headers from body, see RFC5322
	( m_szBody + "\r\n");

}


/// Xxxx xxx...
/***********************************************************************************************//**
 - Description: xx.
 - Arguments:
 @-Returning values:
 @-Note:
 @-Example:
****************************************************************************************************/
bool curl_smtp::curl_Send(void)
{
	struct curl_slist *recipients = NULL;

	// Handle was not initialized........................................................
	if ( !m_curl ){
		throw std::runtime_error( "curl_easy_init() error." );
		return false;
	}


	//Prepare the email with all needed data.............................................
	generateEMail();

	//Use stringCat() to encode and transfer data to a  byte vector(Not yet).............
	//stringCat();

	//Creates the stringdata object with the string......................................
	stringdata textdata( m_szAll );

	//Show output on console....
	//DBG_show();


	// Set options
    //1- Set username and password.......................................................
    curl_easy_setopt( m_curl.get( ), CURLOPT_USERNAME, m_szUsername.c_str() );
    curl_easy_setopt( m_curl.get( ), CURLOPT_PASSWORD, m_szPassword.c_str() );


    /*2- This is the URL for your mailserver. Note the use of port 587 here,
	 * instead of the normal SMTP port (25). Port 587 is commonly used for
	 * secure mail submission (see RFC4403), but you should use whatever
	 * matches your server configuration. */
	curl_easy_setopt( m_curl.get( ), CURLOPT_URL, m_szURL.c_str( ) );

    /*3- In this example, we'll start with a plain text connection, and upgrade
     * to Transport Layer Security (TLS) using the STARTTLS command. Be careful
     * of using CURLUSESSL_TRY here, because if TLS upgrade fails, the transfer
     * will continue anyway - see the security discussion in the libcurl
     * tutorial for more details. */
	//if we use smtp:....:587 this line must be. This does not seem to be needed for 465.
    curl_easy_setopt( m_curl.get( ), CURLOPT_USE_SSL, (long)CURLUSESSL_ALL);

	/*4- If your server doesn't have a valid certificate, then you can disable
	 * part of the Transport Layer Security protection by setting the
	 * CURLOPT_SSL_VERIFYPEER and CURLOPT_SSL_VERIFYHOST options to 0 (false).
	 *   curl_easy_setopt(curl, CURLOPT_SSL_VERIFYPEER, 0L);
	 *   curl_easy_setopt(curl, CURLOPT_SSL_VERIFYHOST, 0L);
	 * That is, in general, a bad idea. It is still better than sending your
	 * authentication details in plain text though.  Instead, you should get
	 * the issuer certificate (or the host certificate if the certificate is
	 * self-signed) and add it to the set of certificates that are known to
	 * libcurl using CURLOPT_CAINFO and/or CURLOPT_CAPATH. See docs/SSLCERTS
	 * for more information. */
	curl_easy_setopt( m_curl.get( ), CURLOPT_CAPATH, m_szCAPATH.c_str() );
	curl_easy_setopt( m_curl.get( ), CURLOPT_CAINFO, m_szCAINFO.c_str() );

	//5-
	curl_easy_setopt( m_curl.get( ), CURLOPT_SSL_VERIFYPEER, 0L);
	curl_easy_setopt( m_curl.get( ), CURLOPT_SSL_VERIFYHOST, 0L);

    /*6- Note that this option isn't strictly required, omitting it will result
     * in libcurl sending the MAIL FROM command with empty sender data. All
     * autoresponses should have an empty reverse-path, and should be directed
     * to the address in the reverse-path which triggered them. Otherwise,
     * they could cause an endless loop. See RFC 5321 Section 4.5.5 for more
     * details.*/
    curl_easy_setopt( m_curl.get( ), CURLOPT_MAIL_FROM, m_szFrom.c_str() );

	/*7- Add two recipients, in this particular case they correspond to the
	 * To: and Cc: addressees in the header, but they could be any kind of
	 * recipient. */
	recipients = curl_slist_append(recipients, m_szTo.c_str() );

	if(m_szCC!="")
		recipients = curl_slist_append(recipients, m_szCC.c_str() );

	curl_easy_setopt( m_curl.get( ), CURLOPT_MAIL_RCPT, recipients);

    /*8- We're using a callback function to specify the payload (the headers and
     * body of the message). You could just use the CURLOPT_READDATA option to
     * specify a FILE pointer to read from. */
    curl_easy_setopt( m_curl.get( ), CURLOPT_READFUNCTION, curl_smtp::read_callback);
    curl_easy_setopt( m_curl.get( ), CURLOPT_READDATA, &textdata );
    curl_easy_setopt( m_curl.get( ), CURLOPT_UPLOAD, 1L);


    /*9- Since the traffic will be encrypted, it is very useful to turn on debug
     * information within libcurl to see what is happening during the transfer. */
	if( m_bDebug )
    	curl_easy_setopt( m_curl.get( ), CURLOPT_VERBOSE, 1L);

	// Perform the request
	m_result = curl_easy_perform( m_curl.get( ) );

    // Check for errors
    if (m_result != CURLE_OK) {
        std::cerr << "curl_easy_perform() failed: " << curl_easy_strerror( m_result ) << "\n";
        return false;
    }


    // Free the list of recipients.......................................................
    curl_slist_free_all(recipients);

    //TODO: This is part of the unique_ptr pair (m_curl). See constructor.
    //curl_easy_cleanup( m_curl.get( ) );


    return true;
}


/// Xxxx xxx...
/***********************************************************************************************//**
 - Description: xx.
 - Arguments:
 @-Returning values:
 @-Note:
 @-Example:
****************************************************************************************************/
bool curl_smtp::curl_Send( const std::string &szEMaddress )
{
	m_szTo = szEMaddress;

	generateEMail();

	if( !curl_Send( ) )
		return false;

	return true;
}


void curl_smtp::DBG_show(){

	std::cout << "---------------------------------------------:\n"  << std::endl;
	std::cout << "----->m_szAll:\n" << m_szAll << std::endl;
	std::cout << "---------------------------------------------:\n"  << std::endl;
	std::cout << "m_szUsername= " << m_szUsername  << std::endl;
	std::cout << "m_szPassword= " << m_szPassword << std::endl;
	std::cout << "m_szURL= " << m_szURL << std::endl;
	std::cout << "m_szCAPATH= " << m_szCAPATH << std::endl;
	std::cout << "m_szCAINFO= " << m_szCAINFO << std::endl;
	std::cout << "---------------------------------------------:\n"  << std::endl;

}

/// Xxxx xxx...
/***********************************************************************************************//**
 - Description: xx.
 - Arguments:
 @-Returning values:
 @-Note:
 @-Example:
****************************************************************************************************/
std::string curl_smtp::get_DateTime( )
{
    const int RFC5322_TIME_LEN = 32;
    time_t t;
    struct tm *tm;

    std::string ret;
    ret.resize(RFC5322_TIME_LEN);

    time(&t);
    tm = localtime(&t);

    strftime(&ret[0], RFC5322_TIME_LEN, "%a, %d %b %Y %H:%M:%S %z", tm);

    return ret;
}



/// Xxx xx...
/***********************************************************************************************//**
 - Description: xx.
 - Arguments:
 @-Returning values:
 @-Note:
 @-Example:
****************************************************************************************************/
std::string curl_smtp::get_MessageId()
{
    const int MESSAGE_ID_LEN = 37;
    time_t t;
    struct tm tm;

    std::string ret;
    ret.resize(15);

    time(&t);
    gmtime_r(&t, &tm);	//On WIN gmtime_s(&tm, &t);

    strftime( const_cast<char *>(ret.c_str()), MESSAGE_ID_LEN, "%Y%m%d%H%M%S.", &tm);

    ret.reserve(MESSAGE_ID_LEN);

    static const char alphanum[] =
            "0123456789"
            "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
            "abcdefghijklmnopqrstuvwxyz";

    while( ret.size() < MESSAGE_ID_LEN ){
        ret += alphanum[rand() % (sizeof(alphanum) - 1)];
    }

    return ret;
}


/// Xxx xx...
/***********************************************************************************************//**
 - Description: xx.
 - Arguments:
 @-Returning values:
 @-Note:
 @-Example:
****************************************************************************************************/
void curl_smtp::setCurlE_Header( const std::string &szAppName, const std::string &szFrom, const std::string &szTo,
								 const std::string &szCC, const std::string &MimeVersion, const std::string &ContentType,
								 const std::string &Charset,const std::string &szSubjet, bool bGenerateID
					)
{
	m_szAppName = szAppName;
	m_szFrom = szFrom;
	m_szTo = szTo;
	m_szCC = szCC;

	if( bGenerateID )
		m_szMessageID = get_MessageId();

	m_szMimeVersion = MimeVersion;
	m_szContentType = ContentType;
	m_szSubject = szSubjet;

}




void curl_smtp::setCurlE_Body( const std::string &szBody )
{
	m_szBody = szBody;
}

std::string curl_smtp::getError()
{
	return m_szError;
}





//Not used yet.
const std::vector<std::uint8_t> &curl_smtp::get_data( ) const
{
	return m_data;
}

//Not used yet.
std::string curl_smtp::get_string( ) const
{
	return std::string( m_data.begin( ), m_data.end( ) );
}















