/*
 *  curl_smtp.hpp
 *
 *  Created on	: 14 mar. 2020
 *  Author		: Casiano Carlos Budden
 *	email		: main.buhoproject@gmail.com, info.xunix@gmail.com,
 *
 *
					GNU GENERAL PUBLIC LICENSE
                       Version 2, June 1991

 Copyright (C) 1989, 1991 Free Software Foundation, Inc.,
 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
 Everyone is permitted to copy and distribute verbatim copies
 of this license document, but changing it is not allowed.
 *
 *
 * Other usefull references:
 * 	https://codereview.stackexchange.com/questions/139784/sending-email-using-libcurl
 */

#ifndef CURL_SMTP_HPP_
#define CURL_SMTP_HPP_



#include <iostream>//Leave this while using stout

#include <time.h>

#include <vector>
#include <memory>
#include <string>
#include <cstring>

#include <curl/curl.h>


#define ENCODE_NONE	0
#define ENCODE_BASE64	1



class stringdata {


public:
    std::string msg;
    size_t bytesleft;

    stringdata( std::string &m ) :
    	msg( m ),
		bytesleft{ msg.size() }
    {}

    ~stringdata(){};


/* Which of both is better??
public:
	std::string msg;
	size_t bytesleft;

	stringdata( std::string &m ) :
		msg{ m },
		bytesleft{ msg.size() }
	{}

	~stringdata( ){delete &msg;}
*/


};

// URL curl extended class
class curl_smtp
{

private:
	//
	std::unique_ptr<CURL, void(*)(CURL*)> m_curl;
	CURLcode m_result;
	std::vector<std::uint8_t> m_data;	//Not used yet.

	std::string m_szError;
	//
	std::string m_szUsername,
				m_szPassword,
				m_szURL,
				m_szCAPATH,
				m_szCAINFO;
	unsigned int m_nEncodeType;
	bool		m_bDebug;

	std::string m_szFrom,
				m_szTo,
				m_szCC,
				m_szAppName,
				m_szSubject,
				m_szMimeVersion,
				m_szContentType,
				m_szCharset,
				m_szMessageID,
				m_szBody;

	std::string m_szAll;




public:
	curl_smtp( const std::string &szUsername, const std::string &szPasswd, const std::string &szUrl,
			   const std::string &szCapath, const std::string &szCainfo, const unsigned int &nEnctype, const bool &bDbg
			 ) :
		m_curl( curl_easy_init( ), [](CURL *c){ curl_easy_cleanup( c ); } ),
		m_result( CURLE_OK ),
		m_szError(""),
		m_szUsername( szUsername ),
		m_szPassword( szPasswd ),
		m_szURL( szUrl ),
		m_szCAPATH( szCapath ),
		m_szCAINFO( szCainfo ),
		m_nEncodeType( nEnctype ),
		m_bDebug( bDbg ),

		m_szFrom(""),
		m_szTo(""),
		m_szCC(""),
		m_szAppName(""),
		m_szSubject(""),
		m_szMimeVersion(""),
		m_szContentType(""),
		m_szCharset(""),
		m_szMessageID(""),
		m_szBody(""),

		m_szAll("")
	{}

	~curl_smtp(){};

private:
	static size_t read_callback( void *contents, size_t size, size_t nmemb, void *userp );
	void stringCat( void );
	void generateEMail();

public:
	bool curl_Send(void);
	bool curl_Send( const std::string &szEMaddress );
	void DBG_show();

	std::string get_DateTime( );
	std::string get_MessageId();
	void setCurlE_Header(
			 const std::string &szAppName, const std::string &szFrom, const std::string &szTo,
			 const std::string &szCC, const std::string &MimeVersion, const std::string &ContentType,
			 const std::string &Charset, const std::string &szSubjet, bool bGenerateID
						);
	void setCurlE_Body( const std::string &szBody );
	std::string getError();




	//Part of an experiment
	const std::vector<std::uint8_t> &get_data( ) const;
	std::string get_string( ) const;




};

#endif /* CURL_SMTP_HPP_ */


