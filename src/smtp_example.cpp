/*
 *  smtp_example.cpp
 *
 *  Created on	: 14 mar. 2020
 *  Author		: Casiano Carlos Budden
 *	email		: main.buhoproject@gmail.com, info.xunix@gmail.com,
 *
 *
					GNU GENERAL PUBLIC LICENSE
                       Version 2, June 1991

 Copyright (C) 1989, 1991 Free Software Foundation, Inc.,
 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
 Everyone is permitted to copy and distribute verbatim copies
 of this license document, but changing it is not allowed.

 *
 */

#include "curl_smtp.hpp"


int main(int argc, char **argv) {


	//Initialize constructor with params...........................................................
	//szUsername: Gmail user of the From e-mail account.
	//szPasswd  : User password related to user.
	//szUrl		: smtp socket. ( "smtp://smtp.gmail.com:587" for gmail usualy)
	//szCapath	: Directory holding CA certificates ( see curl CURLOPT_CAPATH )
	//szCainfo	: Full path to Certificate Authority (CA) bundle. ( see CURLOPT_CAINFO )
	//nEnctype 	: Encoding type to be used in stringCat() method. Not yet developed.
	//bDbg		: Sets the CURLOPT_VERBOSE usage.
	curl_smtp insSMTP( "UserFromEmail@gmail.OrOther", "passwd", "smtpSocket",
						"/path/to/certs/",  "/path/to/certs/TheCertificate.crt", ENCODE_NONE , true);

	//This e-mail could be a different one from the user...........................................
	//szAppName:	The name of the app that sends the email.
	//szFrom:		The from email
	//szTo:			The recipient.
	//szCC:			The copy to other recipient.
	//MimeVersion:	Mime type version.		(E.G.: MimeVersion( "1.0") )
	//ContentType:	The content type.		(E.G.: text/html )
	//Charset:		Charset					(E.G.: "ISO-8859-1" )
	//szSubjet:		The emails subjet.
	//bGenerateID:	An id for the email.	(E.G.: )
	std::string szFrom = "from@some.sender";
	insSMTP.setCurlE_Header( "Buho App.", szFrom, "To@some.one", "copyc@xxxx.net", "", "",
							 "" , "TThis is SMTP TLS example message.", false );


	//E-Mail body..................................................................................
	insSMTP.setCurlE_Body( "It could be a lot of lines, could be MIME encoded, whatever."
							"There are much more data."
							"  - etc1."
							"  - etc2."
							"Check the RFC5322 compliance." );


	//Conversion characters for all string to a <uchar> vector with specified encoding..................................
	//insSMTP.stringCat();

	//Send e-mail...//OR to other address: insSMTP.curl_Send( "some@other.email" );
	if( !insSMTP.curl_Send() )
	{
		std::cout << "curl_easy_perform() faill: " << insSMTP.getError() << "\n";
	}


	return 0;
}




